from flask import Flask, render_template, request
import psycopg2

app = Flask(__name__)
try:
    conn = psycopg2.connect("dbname='contacts' user='postgres' host='localhost' password='1111'")
except:
    print("I am unable to connect to the database")
cur = conn.cursor()


@app.route('/', methods=['GET', 'POST', 'DELETE'])
def home_page():
    if request.method == 'POST':
        user_name = request.form['user_name']
        phone_number = request.form['phone_number']
        query = f'INSERT INTO contacts(user_name, phone_number) VALUES(\'{user_name}\', {phone_number});'
        cur.execute(query)
        conn.commit()
    if request.method == 'DELETE':
        print('jhjg')
        name = request.args.get('name')
        print(name)
    else:
        search = request.args.get('search')
        if search is None:
            cur.execute("""SELECT * from contacts""")
        else:
            query = f'SELECT * from contacts WHERE user_name LIKE \'%{search}%\';'
            cur.execute(query)
        rows = cur.fetchall()

        return render_template('home_page.html', contacts=rows)
